#coding: utf-8

# 第一層標題
m1 = "<a href='/about'>有關本站</a>"
m11 = "<a href='/'>Brython程式區</a>"
m111 = "<a href=''>四連桿機構</a>"
m112 = "<a href=''>零件體積表列</a>"
m12 = "<a href='/creoParts'>Creo 零件</a>"
# 第一層標題
m2 = "<a href=''>期末報告</a>"
m21 = "<a href=''>零件區</a>"
m211 = "<a href=''>參數零件</a>"
m212 = "<a href=''>參數組件</a>"
m22 = "<a href=''>Solvespace 零件</a>"
# 第一層標題
m3 = "<a href=''>參考資料</a>"
m31 = "<a href=''>Creo</a>"
m311 = "<a href=''>零件繪圖</a>"
m312 = "<a href=''>零件組立</a>"
m313 = "<a href=''>Pro/Web.Link</a>"
m32 = "<a href=''>Solid Edge</a>"
m33 = "<a href=''>Solvespace</a>"
m34 = "<a href=''>Python</a>"
# 第一層標題
m4 = "<a href=''>組員介紹</a>"
m41 = "<a href='/introMember1'>第一位</a>"
m42 = "<a href='/introMember2'>第二位</a>"
m43 = "<a href=''>第三位</a>"
m44 = "<a href=''>第四位</a>"

# 請注意, 數列第一元素為主表單, 隨後則為其子表單
表單數列 = [ \
        [m1, [m11, m111, m112], m12],  \
        [m2, [m21, m211, m212], m22], \
        [m3, [m31, m311, m312, m313], m32, m33, m34], \
        [m4, m41, m42, m43, m44]
        ]

def SequenceToUnorderedList(seq, level=0):
    # 只讓最外圍 ul 標註 class = 'nav'
    if level == 0:
        html_code = "<ul class='nav'>"
    # 子表單不加上 class
    else:
        html_code = "<ul>"
    for item in seq:
        if type(item) == type([]):
            level += 1
            html_code +="<li>"+ item[0]
            # 子表單 level 不為 0
            html_code += SequenceToUnorderedList(item[1:], 1)
            html_code += "</li>"
        else:
            html_code += "<li>%s</li>" % item
    html_code += "</ul>\n"
    return html_code

def GenerateMenu():
    return SequenceToUnorderedList(表單數列, 0) \
+'''
<script type="text/javascript" src="/static/jscript/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="/static/jscript/dropdown.js"></script>
'''
